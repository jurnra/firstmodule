<?php
namespace JuistdIT\FirstModule\Block;
use Magento\Framework\View\Element\Template;

class Main extends Template
{
    private $juistditCreator;

    protected function _prepareLayout()
    {
        $this->setMessage('Hello Again World');
        $this->setHetZalWel('Bloemen zijn in de zomer mooier');
        $this->setJuistdITCreator($this->getRequest()->getParam('name'));

    }

    public function getJuistdITCreator()
    {
        return $this->juistditCreator;
    }

    public function setJuistdITCreator($value='Jurn Raaijmakers')
    {
        $this->juistditCreator = $value;
    }
}
